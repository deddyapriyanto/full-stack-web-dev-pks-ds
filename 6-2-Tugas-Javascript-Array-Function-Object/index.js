  

// soal 1
// buatlah variabel seperti di bawah ini

// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// .... jawaban soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
items = daftarHewan.sort()
for (i = 0; i < items.length; i++) 

console.log(items[i])




// soal 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
/* 
    Tulis kode function di sini
*/ 
// var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" } 
// var perkenalan = introduce(data)
// console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// .... jawaban soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = "Nama saya " + data['name'] + ", umur saya " + data['age'] +  " tahun, alamat saya di " + data['address'] + ", dan saya punya hobby yaitu " + data['hobby']

console.log(perkenalan)




// soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
// var hitung_1 = hitung_huruf_vokal("Muhammad")
// var hitung_2 = hitung_huruf_vokal("Iqbal")
// console.log(hitung_1 , hitung_2) // 3 2

// .... jawaban soal 3
var hitung_huruf_vokal = str => Array.from(str)
  .filter(letter => 'AIUEOaiueo'.includes(letter)).length;

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)




// soal 4
// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

// console.log( hitung(0) ) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8

// .... jawaban soal 4
function hitung($angka){
    
    var a = $angka * 2 - 2
    
    return a 
}

console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )