<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::latest()->get();

        return response()->json([
            'success'=>true,
            'message'=>'Data daftar Comment berhasil ditampilkan',
            'data'   =>$comments
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        Mail::to{$commment->post->user->email)->send(new PostAuthorMail($order)};

        if($comment){
            return response()->json([
                'success'   =>true,
                'message'   =>'Data Comment berhasil dibuat',
                'data'      =>$comment
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Comment gagal dibuat'
        ],409);
    }

    public function show(Request $request)
    {
        $comment = Comment::findOrfail($request->id);

        if($comment)
        {
            return response()->json([
                'success'   =>true,
                'message'   =>'Data Post berhasil ditampilkan',
                'data'      =>$comment
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan'
        ],404);
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Post::find($id);

        if($comment)
        {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success'   =>false,
                    'message'   =>'Data post bukan milik user login',
                ],403);     
            }

            $comment->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success'   =>true,
                'message'   =>'Data dengan judul : '. $comment->title .' berhasil diupdate',
                'data'      =>$comment
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Data dengan id : '. $id .' tidak ditemukan'
            ],404);
        }
            
    }

    public function destroy(Request $request)
    {
        $comment = Comment::findOrfail($request->id);

        if($comment)
        {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success'   =>false,
                    'message'   =>'Data post bukan milik user login',
                ],403);     
            }

            $comment->delete();

            return response()->json([
                'success'   =>true,
                'message'   =>'Data Post berhasil didelete',
                'data'      =>$comment
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan'
        ],404);
    }
}
