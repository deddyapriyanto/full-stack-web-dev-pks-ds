<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use App\Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'email'=>'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        User::where('email', $request->email)->first();

        return response()->json([
            'success' => false,
            'message' => 'email tidak ditemukan'
        ],400);

        $user->update([
            'password' => Hash::make($request->password)
        ]);
        
        return response()->json([
            'success' => true,
            'message' => 'password berhasil diubah',
            'data' => $user
        ]);
    }
}
