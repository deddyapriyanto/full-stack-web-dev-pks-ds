<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->get();

        return response()->json([
            'success'=>true,
            'message'=>'Data daftar Role berhasil ditampilkan',
            'data'   =>$roles
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        if($role){
            return response()->json([
                'success'   =>true,
                'message'   =>'Data Role berhasil dibuat',
                'data'      =>$post
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat'
        ],409);
    }

    public function show(Request $request)
    {
        $role = Role::findOrfail($request->id);

        if($role)
        {
            return response()->json([
                'success'   =>true,
                'message'   =>'Data Role berhasil ditampilkan',
                'data'      =>$role
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan'
        ],404);
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $role = Role::find($id);

        if($role)
        {
            $role->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success'   =>true,
                'message'   =>'Data dengan judul : '. $role->title .' berhasil diupdate',
                'data'      =>$role
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Data dengan id : '. $id .' tidak ditemukan'
            ],404);
        }
            
    }

    public function destroy(Request $request)
    {
        $role = Role::findOrfail($request->id);

        if($role)
        {

            $role->delete();

            return response()->json([
                'success'   =>true,
                'message'   =>'Data Post berhasil didelete',
                'data'      =>$role
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan'
        ],404);
    }
}
