<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    rotected $fillable = ['content', 'post_id', 'usr_id'];

    protected $primaryKey ='id';

    protected $keyType ='string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if(empty($model->id)){
                $model->id = Str::uuid();
            }

        });


    }
}
